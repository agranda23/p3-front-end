Este proyecto fue creado con [Create React App](https://github.com/facebook/create-react-app).

Para instalar las dependencias del proyecto se debe ejecutar el comando `npm install`.

## Scripts Disponibles

En el directorio del proyecto, se pueden ejecutar:

### `npm start`

Ejecuta la aplicación en modo desarrollo.<br>
Abrir [http://localhost:3000](http://localhost:3000) para ver en el navegador.

NOTA: Para que el sistema funcione adecuadamente, la variable PEC3_KEY del archivo .env debe tener el mismo valor que la variable KEY del archivo .env del proyecto `p3-back-end`
