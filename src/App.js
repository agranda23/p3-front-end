import React, { Component } from 'react';
import Login from './authentication/login';
import './App.css';

const initalState = {'autenticated': false, 'message': '', 'name': ''};

class App extends Component {
  constructor(props) {
    super(props);

    this.state = initalState;
    this.updateSessionInfo = this.updateSessionInfo.bind(this);
  }

  updateSessionInfo(newInfo) {
    this.setState({
      'autenticated': newInfo.autenticated,
      'name': newInfo.fullName,
      'message': newInfo.message || '',
    });
  }

  renderUserInfo(name) {
    return (
      <div>
        <h1>Saludos! {name}</h1>
      </div>
    );
  };

  render() {
    const {autenticated, message, name} = this.state;
    
    return (
      <div className="App">
        {!autenticated && <Login updateSessionInfo={this.updateSessionInfo} />}
        <br/>
        {message && <h2 className='message'>{message}</h2>}
        {name && this.renderUserInfo(name)}        
      </div>
    );
  }
}

export default App;
