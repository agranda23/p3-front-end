import React, { Component } from 'react';
import {encrypt} from '../common/utils/cryptography';
import {doPostRequest} from '../common/utils/apiCall';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      'username': '',
      'password': '',
      'cryptedUsername': '',
      'cryptedPassword': '', 
    }

    this.onUsernameChange = this.onUsernameChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.onLoginSubmit = this.onLoginSubmit.bind(this);
  }

  async onLoginSubmit(event) {
    event.preventDefault();
    const {cryptedUsername, cryptedPassword} = this.state;
    const sessionInfo = await doPostRequest('http://localhost:3001/login', {
      'username': cryptedUsername,
      'password': cryptedPassword,
    });
    
    if (sessionInfo) {
      this.props.updateSessionInfo(sessionInfo);
    }
  }

  onUsernameChange(event) {
    this.setState({
      'username': event.target.value,
      'cryptedUsername': encrypt(event.target.value)
    });
  }

  onPasswordChange(event) {
    this.setState({
      'password': event.target.value,
      'cryptedPassword': encrypt(event.target.value)
    });
  }

  render() {
    const {username, password} = this.state;

    return (
      <form onSubmit={this.onLoginSubmit}>
        <h2>Login</h2>
        <input type='text' value={username} onChange={this.onUsernameChange} />
        <br />
        <input type='password' value={password} onChange={this.onPasswordChange} />
        <br />
        <br />
        <input type='submit' defaultValue='Ingresar' />
      </form>
    );
  }
}

export default Login;
