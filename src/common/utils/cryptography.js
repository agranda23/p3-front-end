import encryptor from 'simple-encryptor';

const key = process.env.PEC3_KEY;
const helper = encryptor(key);

export const encrypt = (plainText) => {
  return helper.encrypt(plainText);
};
