import axios from 'axios';

export const doPostRequest = (url, params) => {
  return axios.post(url, params)
  .then(function (response) {
    return response.data;
  })
  .catch(function () {
    return null;
  });
};
